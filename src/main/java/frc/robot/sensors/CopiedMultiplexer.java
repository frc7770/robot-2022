package frc.robot.sensors;

import com.revrobotics.ColorSensorV3;
import com.revrobotics.ColorSensorV3.RawColor;


import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;


public class CopiedMultiplexer {
    private final int kMultiplixerAddress = 0x70;
    private static I2C multiplexer;
    private ColorSensorV3 sensor;
    private final int port;

    public CopiedMultiplexer(I2C.Port i2cPort, int port) {
        if (multiplexer == null) {
            multiplexer = new I2C(i2cPort, kMultiplixerAddress);
        }

        this.port = port;
        setChannel();
        sensor = new ColorSensorV3(i2cPort);
    }

    private void setChannel() {
        multiplexer.write(kMultiplixerAddress, 1 << port);
    }

    public Color getColor() {
        setChannel();
        return sensor.getColor();
    }

    public int getProx() {
        setChannel();
        return sensor.getProximity();
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("BallColor"+port, getColor().red);
        System.out.println("BALL PROX " + port + " : " + getProx());
        System.out.println("BALL RED " + port + " : " + getColor().red);
    }
}
