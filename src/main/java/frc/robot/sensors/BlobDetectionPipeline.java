package frc.robot.sensors;

import org.opencv.core.MatOfKeyPoint;
import edu.wpi.first.vision.VisionPipeline;

public interface BlobDetectionPipeline extends VisionPipeline {
    public MatOfKeyPoint findBlobsOutput();
}
