package frc.robot.sensors;

import com.revrobotics.ColorSensorV3;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class MultiPlexedColorSensor extends ColorSensor {
    private final int port;
    private static I2C multiplexer;
    private final int kMultiplixerAddress = 0x70;
    public MultiPlexedColorSensor(int port) {
        if (multiplexer == null) {
            multiplexer = new I2C(i2cPort, kMultiplixerAddress);
        }
        this.port = port;
        setChannel();
        m_colorSensor = new ColorSensorV3(i2cPort);
    }

    private void setChannel() {
        multiplexer.write(kMultiplixerAddress, 1 << port);
    }

    @Override
    public ColorSensor.BallColor getBall() {
        setChannel();
        return super.getBall();
    }

    public void logToDashboard() {
        setChannel();
        SmartDashboard.putString("BallColor " + port, getBall().name());
    }
}
