package frc.robot.sensors;

import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;

public class LimeLight {
    double Y_SWEET_SPOT = 13.5;
    double X_SWEET_SPOT = 5;
    // If target detects a target
    public boolean detectTarget() {
        double tv = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tv").getDouble(0);
        // Testing for if the limelight finds a target
        if (tv == 1) {
            return true;
        }
        else {
            return false;
        }      
    }
    // x angle of the target
    public double getXAngle() {
        double tx = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tx").getDouble(0);
        return tx - X_SWEET_SPOT; 
    }
    // y angle of the target
    public double getYAngle() {
        double ty = NetworkTableInstance.getDefault().getTable("limelight").getEntry("ty").getDouble(0);
        return ty - Y_SWEET_SPOT;  
    }
    public boolean isSweetSpot() {
        return ((Math.abs(getXAngle()) < 3) && (Math.abs(getYAngle()) < 2));
    }
    // Logging the values to the dashboard
    public void logToDashboard() {
        SmartDashboard.putBoolean("LimeLightV", detectTarget());
        SmartDashboard.putNumber("LimeLightX", getXAngle());
        SmartDashboard.putNumber("LimeLightY", getYAngle());
        SmartDashboard.putBoolean("Shooter Sweet Spot", isSweetSpot());
    }
}
