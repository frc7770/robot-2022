package frc.robot.sensors;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;


public class Potentiometer {
    AnalogInput analog = new AnalogInput(0);

    public double getAngle() {
        double voltage = analog.getVoltage();
        return (voltage - 2.5) * 54;
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Potentiometer Angle", getAngle());
    }
}
