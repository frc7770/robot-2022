package frc.robot.sensors;

import javax.sound.sampled.Port;

import org.opencv.core.KeyPoint;
import org.opencv.core.MatOfKeyPoint;

import edu.wpi.first.cscore.VideoSource;
import edu.wpi.first.vision.VisionThread;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class VisionThreadSensor {
    private VisionThread visionThread;
    private boolean hasTarget = false;
    private double distance = 0.0;
    private double angle = 0.0;
    public VisionThreadSensor(VideoSource videoSource, BlobDetectionPipeline videopipeline) {
        visionThread = new VisionThread(videoSource, videopipeline , pipeline -> {    
        if (!pipeline.findBlobsOutput().empty()) {
            hasTarget = true;
            KeyPoint point = getBestTargetPoint(pipeline.findBlobsOutput());
            SmartDashboard.putString("Blue Blob", point.toString());
            SmartDashboard.putNumber("Position", point.pt.x);
            SmartDashboard.putNumber("Size", point.size);
            // Get angle
            double centeredReading = point.pt.x - 160;
            angle = centeredReading/6.1;
            // Get distance
            distance = 9.179 * Math.pow(.986, point.size);
        }
        else {
            hasTarget = false;
            distance = 0.0;
            angle = 0.0;
        }
        SmartDashboard.putNumber("Camera Ball Distance", distance);
        SmartDashboard.putNumber("Camera Ball Angle", angle);
        SmartDashboard.putBoolean("Has Target", hasTarget);
        });
        visionThread.start();
    }

    public KeyPoint getBestTargetPoint(MatOfKeyPoint blobs) {
        // need a better algorithm.  Maybe find the biggest blob?
        return blobs.toArray()[0];
    }

    public boolean hasTarget() {
        return hasTarget;
    }

    public double getDistance() {
        return distance;
    }

    public double getAngle() {
        return angle;
    }
}
