// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.sensors;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;

import com.revrobotics.ColorMatch;
import com.revrobotics.ColorMatchResult;

/** Add your docs here. */
public class ColorSensorOld {

    protected final I2C.Port i2cPort = I2C.Port.kOnboard;
    protected ColorSensorV2 m_colorSensor = new ColorSensorV2(i2cPort);
    private final ColorMatch m_colorMatcher = new ColorMatch();
    public static boolean hasBall = false;

    public static enum BallColor{RED, BLUE, OTHER, NONE}

    public ColorSensorOld() {
        m_colorMatcher.addColorMatch(Color.kBlue);
        m_colorMatcher.addColorMatch(Color.kRed);
    }

    public ColorSensor.BallColor getBall() {
        Color detectedColor = m_colorSensor.getColor();
        ColorMatchResult match = m_colorMatcher.matchClosestColor(detectedColor);
        // If low confidence return none
        SmartDashboard.putNumber("Confidence", match.confidence);
        if (match.confidence < .425) {
            hasBall = false;
            return ColorSensor.BallColor.NONE;
        }
        if(match.color == Color.kBlue && match.confidence > .4) {
            hasBall = true;
            return ColorSensor.BallColor.BLUE;
        }
        else if(match.color == Color.kRed && match.confidence > .5) {
            hasBall = true;
            return ColorSensor.BallColor.RED;
        }
        else 
            hasBall = true;
            return ColorSensor.BallColor.OTHER;
    }

    public Color getColor() {
        return m_colorSensor.getColor();
    }

    public void logToDashboard() {
        SmartDashboard.putString("OldBallColor", getBall().name());
        SmartDashboard.putBoolean("Old Has Ball", hasBall);
    }

    public static boolean hasBall() {
        return hasBall;
    }
}
