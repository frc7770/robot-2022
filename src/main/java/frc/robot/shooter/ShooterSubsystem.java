package frc.robot.shooter;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;
import edu.wpi.first.wpilibj2.command.SubsystemBase;


/**
 * Spins a pair of wheels that launches the ball at the target
 */
public class ShooterSubsystem extends SubsystemBase {
    public MotorControllerGroup m_shooter;
    public RelativeEncoder shooterMasterEncoder;
    public RelativeEncoder shooterSlaveEncoder;

    public ShooterSubsystem() {
        CANSparkMax shooterMaster = new CANSparkMax(Constants.SHOOTER_LEFT, CANSparkMax.MotorType.kBrushless);
        CANSparkMax shooterSlave = new CANSparkMax(Constants.SHOOTER_RIGHT, CANSparkMax.MotorType.kBrushless);
        if ((shooterMaster != null) && (shooterSlave != null)) {
            shooterSlave.setInverted(true);
            m_shooter = new MotorControllerGroup(shooterMaster, shooterSlave);
        }
        if (shooterMaster != null) {
            shooterMasterEncoder = shooterMaster.getEncoder();
            shooterSlaveEncoder = shooterSlave.getEncoder();
        }
    }

    public void setSpeed() {
        if (m_shooter != null) {
            m_shooter.set(Constants.SHOT_SPEED);
        }
    }

    public void setLowSpeed() {
        if (m_shooter != null) {
            m_shooter.set(Constants.LOWER_SHOT_SPEED);
        }
    }

    public void stop() {
        if (m_shooter != null) {
            m_shooter.set(0);
        }
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Shooter Motor Speed", shooterMasterEncoder.getVelocity());
        SmartDashboard.putNumber("Shooter Slave Speed", shooterSlaveEncoder.getVelocity());
    }

}

