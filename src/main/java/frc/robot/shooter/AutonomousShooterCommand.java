/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.shooter;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class AutonomousShooterCommand extends CommandBase {
  public AutonomousShooterCommand() {
    addRequirements(Robot.shooter);
  }
  @Override
  public void execute() {
    if (!(Robot.shootLow)) {
      Robot.shooter.setSpeed();
    }
    else {
      Robot.shooter.setLowSpeed();
    }
  }

    @Override
    public void end(boolean interrupted) {
       Robot.shooter.stop(); 
    }
}
