/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.shooter;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class XboxShooterCommand extends CommandBase {
  private boolean isRunning = false;
  public XboxShooterCommand() {
    addRequirements(Robot.shooter);
  }
  @Override
  public void execute() {
    if (!(isRunning)) {
      Robot.shooter.setSpeed();
      isRunning = true;
    }
    else {
      Robot.shooter.stop();
      isRunning = false;
    }
  }

  @Override
  public boolean isFinished() {
    return true;
  }
}
