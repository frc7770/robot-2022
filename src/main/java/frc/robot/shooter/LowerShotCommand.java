package frc.robot.shooter;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class LowerShotCommand extends CommandBase {
    public LowerShotCommand() {
        addRequirements(Robot.shooter);
    }
    @Override
    public void execute() {
        if (!(Robot.shootLow)) {
            Robot.shootLow = true;
        }
        else {
            Robot.shootLow = false;
        }
    }
    @Override
    public boolean isFinished() {
        return true;
    }
}