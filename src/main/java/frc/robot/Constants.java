/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

public class Constants {
  //Motors
  //DriveMotors
  public static final int DRIVE_LEFT_FRONT = 20; //20; 
  public static final int DRIVE_LEFT_BACK = 12; //12;
  public static final int DRIVE_RIGHT_FRONT = 31; //31;
  public static final int DRIVE_RIGHT_BACK = 50; //50;
  //ShooterMotors
  public static final int SHOOTER_RIGHT = 16;
  public static final int SHOOTER_LEFT = 15;
  //ClimberMotors
  public static final int CLIMBER_RIGHT = 4; //62 old controller ID
  public static final int CLIMBER_LEFT = 13; //51 old controller ID
  //TurretMotors
  public static final int TURRET = 18;
  //IntakeMotors
  public static final int INTAKE_MOTOR = 40;
  //EsophagusMotors
  public static final int ESOPHAGUS_MOTOR = 60;
  //Controllers
  public static final int DRIVE_CONTROLLER = 0;
  public static final int COPILOT_DRIVE_CONTROLLER = 1;
  public static final int CALLOBRATION_CONTROLELR = 2;
  //Speeds
  public static final double SHOT_SPEED = 1;
  public static final double LOWER_SHOT_SPEED = .5;
  public static final double BANK_SPEED = -.4;
  public static final double INTAKE_SPEED = 0.65;
  public static final double CLIMBER_SPEED = 1;
  //Deadbands
  public static final double DRIVE_DEADBAND = .175;
  public static final double INTAKE_POSITION_DEADBAND = .05;

  //Encoder Clicks Per Foot
  public static final double DRIVE_ENCODER_RATIO = 6.5;
  
  //Encoder Clickers Per Degree
  public static final double DRIVE_ROTATION_ENCODER_RATIO = .122;

  //Esophagus speed
  public static final double ESOPHAGUS_SPEED = .8;

  // Pneumatic Hub ID
  public static final int PNEUMATIC_HUB_ID = 2;
  //Team
  //public static final boolean TEAM_BLUE = true;

  // CL #51
  // DL1 #31
  // DL2 #50
  // DR2 #12
  // DR1 #20
  // CR #62
}