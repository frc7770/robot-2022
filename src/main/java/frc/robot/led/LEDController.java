package frc.robot.led;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.util.Color;

/**
 * Class to initialize a string of LEDs and cycle patterns
 * Some examples: https://www.tweaking4all.com/hardware/arduino/adruino-led-strip-effects/
 */
public class LEDController {

    private AddressableLEDBuffer m_ledBuffer;
    private AddressableLED m_led;
    private int repeatIndex = 0;
    private boolean direction = true;

    /**
     * Constructor to create an LED Controller.
     * @param port PWM port
     * @param numLEDs Length of the LED string
     */
    public LEDController(int port, int numLEDs) {
        // Must be a PWM header, not MXP or DIO
        m_led = new AddressableLED(port);

        // Reuse buffer
        // Default to a length of 60, start empty output
        // Length is expensive to set, so only set it once, then just update data
        m_ledBuffer = new AddressableLEDBuffer(numLEDs);
        m_led.setLength(m_ledBuffer.getLength());

        // Set the data
        //sixAndOnePattern(Color.kGreen, Color.kBlack);
        //m_led.start();
    }

    public void start() {
        m_led.start();
    }

    public void stop() {
        m_led.stop();
    }

    /**
     * Create a moving pattern with six color a and one of color b;
     * @param six
     * @param one
     */
    public void sixAndOnePatternOld(Color six, Color one) {
        // mirror pattern on both halves
        int half = (m_ledBuffer.getLength()/2) + (m_ledBuffer.getLength()%2);
        // For every pixel
        for (var i = 0; i < half; i++) {
            Color color = six;
            if((i%7)==repeatIndex) {
                color = one;
            }
            m_ledBuffer.setLED(i, one);
            m_ledBuffer.setLED(m_ledBuffer.getLength()-i, color);
        }

        // Increase by to make the rainbow "move"
        repeatIndex += 1;
        // Check bounds
        repeatIndex %= 7;

        // Set the data
        m_led.setData(m_ledBuffer);
    }

    /**
     * Battlestar Galactica and Knight Rider pattern
     * @param startLed First led in the scanner
     * @param endLed Last led in the scanner
     * @param cylonColor Scanner color.  Non lit up part will be black.
     * @param fillColor Color for the rest
     */
    public void cylon(int startLed, int endLed, Color cylonColor, Color fillColor) {
        // fill all
        for (var i = 0; i < m_ledBuffer.getLength(); i++) {
            // Set the value
            m_ledBuffer.setLED(i, fillColor);
        }
        // handle patterns that wrap around the end of the led string
        if(startLed>endLed) {
            endLed = endLed+m_ledBuffer.getLength();
        }
        // set the scanner to black
        for (var i = startLed; i < endLed; i++) {
            // Set the value
            m_ledBuffer.setLED((i%m_ledBuffer.getLength()), Color.kBlack);
        }
        // light up the scanner
        m_ledBuffer.setLED(repeatIndex, cylonColor);
        m_ledBuffer.setLED(repeatIndex+1, cylonColor);
        // figure out the repeat
        if(repeatIndex==startLed) {
            // at the start led
            direction=true;
            repeatIndex++;
        } else if(repeatIndex==(endLed-1)) {
            // at the end
            direction=false;
            repeatIndex--;
        } else if(direction) {
            repeatIndex++;
        } else {
            repeatIndex--;
        }
        // Check bounds
        repeatIndex %= m_ledBuffer.getLength();
        // Set the data
        m_led.setData(m_ledBuffer);
    }

    public void rainbow() {
        // For every pixel
        for (var i = 0; i < m_ledBuffer.getLength(); i++) {
            // Calculate the hue - hue is easier for rainbows because the color
            // shape is a circle so only one value needs to precess
            final var hue = (repeatIndex + (i * 180 / m_ledBuffer.getLength())) % 180;
            // Set the value
            m_ledBuffer.setHSV(i, hue, 255, 128);
        }
        // Increase by to make the rainbow "move"
        repeatIndex += 3;
        // Check bounds
        repeatIndex %= 180;
        // Set the LEDs
        m_led.setData(m_ledBuffer);
    }

    public void solid(Color color) {
        stop();
        start();
        // fill all
        for (var i = 0; i < m_ledBuffer.getLength(); i++) {
            // Set the value
            m_ledBuffer.setLED(i, color);
        }
        m_led.setData(m_ledBuffer);
    }

    public void flash(Color color) {
        if (repeatIndex % 10 < 5) {
            // fill all
            for (var i = 0; i < m_ledBuffer.getLength(); i++) {
                // Set the value
                m_ledBuffer.setLED(i, color);
            }
        }
        else {
            // fill all
            for (var i = 0; i < m_ledBuffer.getLength(); i++) {
                // Set the value
                m_ledBuffer.setLED(i, Color.kBlack);
            }
        }
        repeatIndex++;
        m_led.setData(m_ledBuffer);

    }

    public void twoBlueOneYellow() {
        stop();
        // For every pixel
        for (var i = 0; i < m_ledBuffer.getLength(); i++) {
            Color color = Color.kBlue;
            if(i%3 == 0) {
                color = Color.kYellow;
            }
            m_ledBuffer.setLED(i, color);
        }
        // Set the data
        m_led.setData(m_ledBuffer);
        start();
    }

    public void elevenYellowElevenBlue() {
        stop();
        // For every pixel
        for (var i = 0; i < m_ledBuffer.getLength(); i++) {
            Color color = Color.kYellow;
            if(i > (m_ledBuffer.getLength()/2)) {
                color = Color.kYellow;
            }
            m_ledBuffer.setLED(i, color);
        }
        // Set the data
        m_led.setData(m_ledBuffer);
        start();
    }

    public void standbyColor() {
        stop();
        solid(Color.kGreen);
        start();
    }
}
