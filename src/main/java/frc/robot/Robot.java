// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.cscore.UsbCamera;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.climber.ClimberSubsystem;
import frc.robot.climber.XboxClimberCallobrateCommand;
import frc.robot.climber.ZeroClimberPositionCommand;
import frc.robot.drive.AutoDriveCommand;
import frc.robot.drive.AutoRotateCommand;
import frc.robot.drive.AutonomousCameraCommand;
import frc.robot.drive.AutonomousLimeLightCommand;
import frc.robot.drive.DriveSubsystem;
import frc.robot.drive.JoystickDriveCommand;
import frc.robot.esophagus.AutonomousEsophagusCommand;
import frc.robot.esophagus.AutonomousLoadCommand;
import frc.robot.esophagus.EsophagusSubsystem;
import frc.robot.esophagus.LoadRollCommand;
import frc.robot.esophagus.NewAutonomousLoadCommand;
import frc.robot.esophagus.ReverseEsophagusCommand;
import frc.robot.esophagus.StopEsophagusCommand;
import frc.robot.esophagus.XboxEsophagusCommand;
import frc.robot.intake.AutonomousIntakeCommand;
import frc.robot.intake.IntakeSubsystem;
import frc.robot.intake.NewAutonomousIntakeCommand;
import frc.robot.intake.XboxIntakeCommand;
import frc.robot.led.LEDController;
import frc.robot.pneumatics.PneumaticsSubsystem;
import frc.robot.pneumatics.PneumaticsToggleClimberCommand;
import frc.robot.pneumatics.PneumaticsToggleIntakeCommand;
import frc.robot.pneumatics.SetIntakeDown;
import frc.robot.sensors.BallGripPipeline;
import frc.robot.sensors.BallGripPipelineTwo;
import frc.robot.sensors.ColorSensor;
import frc.robot.sensors.ColorSensorOld;
import frc.robot.sensors.LimeLight;
import frc.robot.sensors.Potentiometer;
import frc.robot.sensors.VisionThreadSensor;
import frc.robot.shooter.AutonomousShooterCommand;
import frc.robot.shooter.LowerShotCommand;
import frc.robot.shooter.ShooterSubsystem;
import frc.robot.shooter.XboxShooterCommand;
import frc.robot.turret.TurretSubsystem;

/**
 * This is a demo program showing the use of the DifferentialDrive class. Runs the motors with
 * arcade steering.
 */
public class Robot extends TimedRobot {
  // Team Color
  public static boolean isBlue = false;
  public static boolean shootLow = false;
  public static Color team = Color.kGreen;
  // Sensors
  public static ColorSensor colorSensor0 = new ColorSensor();
  public static ColorSensorOld colorSensor1 = new ColorSensorOld();
  public static LimeLight limeLight = new LimeLight();
  public static BallGripPipeline blueBlobDetector = new BallGripPipeline(); 
  public static VisionThreadSensor vision;
  public static LEDController turretLEDs = new LEDController(0, 75);
  //public static LEDController signLEDs = new LEDController(1, 14);
  //public static LEDController esophagusLEDs = new LEDController(2, 22);
  public static Potentiometer potentiometer = new Potentiometer();
  public static UsbCamera camera0;
  // Controls
  public static XboxController xboxController1 = new XboxController(Constants.COPILOT_DRIVE_CONTROLLER);
  public static Joystick joystick = new Joystick(Constants.DRIVE_CONTROLLER);
  public static XboxController xboxCallobrator = new XboxController(Constants.CALLOBRATION_CONTROLELR);
  // Subsystems
  public static DriveSubsystem drive;
  public static EsophagusSubsystem esophagus;
  public static PneumaticsSubsystem pneumatics;
  public static ShooterSubsystem shooter;
  public static TurretSubsystem turret;
  public static IntakeSubsystem intake;
  public static ClimberSubsystem climber;
  // Xbox Controller Buttons 
  public static JoystickButton aButton = new JoystickButton(xboxController1, 1);
  public static JoystickButton bButton = new JoystickButton(xboxController1, 2);
  public static JoystickButton xButton = new JoystickButton(xboxController1, 3);
  public static JoystickButton yButton = new JoystickButton(xboxController1, 4);
  public static JoystickButton lbButton = new JoystickButton(xboxController1, 5);
  public static JoystickButton rbButton = new JoystickButton(xboxController1, 6);
  public static JoystickButton backButton = new JoystickButton(xboxController1, 7);
  public static JoystickButton startButton = new JoystickButton(xboxController1, 8);
  public static JoystickButton leftJoystickPress = new JoystickButton(xboxController1, 9);
  public static JoystickButton rightJoystickPress = new JoystickButton(xboxController1, 10);
  // Joystick Controller Buttons
  public static JoystickButton triggerButton = new JoystickButton(joystick, 1);
  public static JoystickButton downButton = new JoystickButton(joystick, 2);
  public static JoystickButton leftButton = new JoystickButton(joystick, 3);
  public static JoystickButton rightButton = new JoystickButton(joystick, 4);
  public static JoystickButton leftTopLeft = new JoystickButton(joystick, 5);
  public static JoystickButton leftTopMiddle  = new JoystickButton(joystick, 6);
  public static JoystickButton leftTopRight = new JoystickButton(joystick, 7);
  public static JoystickButton leftBottomRight = new JoystickButton(joystick, 8);
  public static JoystickButton leftBottomMiddle = new JoystickButton(joystick, 9);
  public static JoystickButton leftBottomLeft = new JoystickButton(joystick, 10);
  public static JoystickButton rightTopRight = new JoystickButton(joystick, 11);
  public static JoystickButton rightTopMiddle  = new JoystickButton(joystick, 12);
  public static JoystickButton rightTopLeft = new JoystickButton(joystick, 13);
  public static JoystickButton rightBottomLeft = new JoystickButton(joystick, 14);
  public static JoystickButton rightBottomMiddle = new JoystickButton(joystick, 15);
  public static JoystickButton rightBottomRight = new JoystickButton(joystick, 16);
  
  // Commands
  private Command autonomousCommand;

  
  @Override
  public void robotInit() {
    // This may not get accurate info depending on when the bot
    // was turned on so call it in autonomousInit also.
    initMatchInfo();
    // Initialize Subsystems
    drive = new DriveSubsystem();
    esophagus = new EsophagusSubsystem();
    pneumatics = new PneumaticsSubsystem();
    shooter = new ShooterSubsystem();
    turret = new TurretSubsystem();
    intake = new IntakeSubsystem();
    climber = new ClimberSubsystem();

    //drive.setDefaultCommand(new XboxDriveCommand());
    drive.setDefaultCommand(new JoystickDriveCommand());
    climber.setDefaultCommand(new XboxClimberCallobrateCommand());
    //turret.setDefaultCommand(new LimeLightTurretCommand());
    //drive.setDefaultCommand(new LimeLightAutoRotateCommand());
    //intake.setDefaultCommand(new XboxIntakeCommand());
    camera0 = CameraServer.startAutomaticCapture(0);
    camera0.setResolution(320, 180);
    //UsbCamera camera1 = CameraServer.startAutomaticCapture(1);
   
    turretLEDs.solid(Color.kGreen);
    //signLEDs.twoBlueOneYellow();
    //esophagusLEDs.elevenYellowElevenBlue();
  }

  @Override
  public void robotPeriodic() {
    // Start the command scheduler
    CommandScheduler.getInstance().run();
    // Logging
    colorSensor0.logToDashboard();
    colorSensor1.logToDashboard();
    limeLight.logToDashboard();
    drive.logToDashboard();
    shooter.logToDashboard();
    //intake.logToDashboard();
    //turret.logToDashboard();
    pneumatics.logToDashboard();
    climber.logToDashboard();
    potentiometer.logToDashboard();
  }

  @Override
  public void teleopInit() {
    initMatchInfo();
    turretLEDs.solid(team);
    //signLEDs.twoBlueOneYellow();
    //esophagusLEDs.elevenYellowElevenBlue();
    // Set up buttons
    configureButtonBindings();

    //drive.setDefaultCommand(new XboxDriveCommand());
    //shooter.setDefaultCommand(new XboxShooterCommand());
  }

  @Override
  public void teleopPeriodic() {
    Scheduler.getInstance().run();
  }

  @Override
  public void teleopExit() {
    turretLEDs.standbyColor();
  }

  private void configureButtonBindings() {
    // Joystick Pneumatic Commands
    // Pneumatic Climber Toggle
    leftButton.whenPressed(new PneumaticsToggleClimberCommand());
    // Pneumatics Intake Toggle
    rightButton.whenPressed(new PneumaticsToggleIntakeCommand());
    // Zero Climber Position
    downButton.whenPressed(new ZeroClimberPositionCommand());


    // Xbox Ball commands
    // Xbox Controller Turn on intake
    bButton.whenPressed(new XboxIntakeCommand());
    // Xbox Controller Turn on esophagus
    xButton.whenPressed(new XboxEsophagusCommand());
    // Xbox Controller Turn on shooter
    yButton.whenPressed(new XboxShooterCommand());
    // Xbox Controller Slow Intake
    rbButton.whenPressed(loadBallGroup());
    // Xbox Controller Reverse Intake 
    aButton.whenPressed(new ReverseEsophagusCommand());
    // Xbox Controller Full Shoot Sequence
    lbButton.whenPressed(createShooterGroup());


    // Joystick Ball Commands   
    // JS Turn on intake
    leftTopLeft.whenPressed(new XboxIntakeCommand());
    // JS Turn on esophagus
    leftTopMiddle.whenPressed(new XboxEsophagusCommand());
    // JS Turn on shooter
    leftTopRight.whenPressed(new XboxShooterCommand()); 
    // Turn on slow intake  
    leftBottomMiddle.whenPressed(loadBallGroup());
    // JS Reverse Intake
    leftBottomRight.whenPressed(new ReverseEsophagusCommand());
    // JS Stop esophagus
    leftBottomLeft.whenPressed(new StopEsophagusCommand());
    // JS Toggle to lower hub shot speed
    rightTopRight.whenPressed(new LowerShotCommand());

    // Joystick Full Shooter Command
    triggerButton.whenPressed(createShooterGroup());
  }
  @Override
  public void autonomousInit() {
    initMatchInfo();
    turretLEDs.solid(team);
    vision = new VisionThreadSensor(camera0, new BallGripPipelineTwo());

    CommandScheduler.getInstance().schedule(
      new SequentialCommandGroup(
        new SetIntakeDown(),
        new WaitCommand(.25),
        new AutonomousLimeLightCommand(),
        autoCreateShooterGroup(),
        new ParallelCommandGroup(
          new AutonomousCameraCommand(),
          loadBallGroup()
        ),
        new AutonomousLimeLightCommand(),
        autoCreateShooterGroup()
        /* new AutoRotateCommand(potentiometer.getAngle()),
        new ParallelCommandGroup(
          new AutonomousCameraCommand(),
          loadBallGroup()
        ),
        new AutonomousLimeLightCommand(),
        autoCreateShooterGroup() */
      )
    );
  }

  @Override
  public void autonomousPeriodic() {
    //CommandScheduler.getInstance().run();
  }

  @Override
  public void autonomousExit() {
    if (this.autonomousCommand != null) {
      turretLEDs.standbyColor();
      this.autonomousCommand.cancel();
    }
  }

  public ParallelCommandGroup createShooterGroup() {
    return new ParallelCommandGroup(
      new AutonomousShooterCommand().withTimeout(3),
      new NewAutonomousIntakeCommand().withTimeout(3),
      new SequentialCommandGroup(
        new WaitCommand(.75),
        new AutonomousEsophagusCommand().withTimeout(2.25)
      )
    );
  }

  public ParallelCommandGroup autoCreateShooterGroup() {
    return new ParallelCommandGroup(
      new AutonomousShooterCommand().withTimeout(2.25),
      new NewAutonomousIntakeCommand().withTimeout(2.25),
      new SequentialCommandGroup(
        new WaitCommand(.75),
        new AutonomousEsophagusCommand().withTimeout(1.5)
      )
    );
  }

  public ParallelCommandGroup loadBallGroup() {
    return new ParallelCommandGroup(
      new AutonomousIntakeCommand(),
      new SequentialCommandGroup (
        new WaitCommand(.5),
        new AutonomousLoadCommand(),
        new WaitCommand(.5),
        new LoadRollCommand().withTimeout(.2)
      )
    );
  }

  /**
   * We can not depend on setting match info in robotInit so here is a helper method to 
   * call in autonomousInit.
   */
  private void initMatchInfo() {
    // Team Color
    if (DriverStation.getAlliance().name().equals("Blue")) {
      isBlue = true;
      team = Color.kBlue;
    }
    else {
      team = Color.kRed;
    }
    SmartDashboard.putBoolean("Is Blue", isBlue);
    SmartDashboard.putString("Team Color", DriverStation.getAlliance().name()); 

  }
}
