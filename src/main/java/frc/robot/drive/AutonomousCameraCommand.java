/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.drive;
import org.ietf.jgss.GSSContext;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class AutonomousCameraCommand extends CommandBase {
  boolean isFinished = false;
  double rKP = .015;
  double rKI = 0;
  double rKD = .0025;
  PIDController rotateController = new PIDController(rKP, rKI, rKD);
  double sKP = .1;
  double sKI = 0;
  double sKD = .01;
  PIDController speedController = new PIDController(sKP, sKI, sKD);
  /**
   * Distance in feet
   * @param distance
   */
  public AutonomousCameraCommand() {
    System.out.println("Created AutonmousCamera");
    addRequirements(Robot.drive);
  }
  @Override
  public void execute() {
    System.out.println("Started Command");
    double speed = 0;
    double rotate = 0; 
    if (Robot.vision.hasTarget()) {
      System.out.println("Entered command");
      double rError = Robot.vision.getAngle();
      double dError = Robot.vision.getDistance();

      rotate = rotateController.calculate(-rError, 0);
      speed = speedController.calculate(-dError, 0);
    }
    else {
      speed = .3;
    }
    

    SmartDashboard.putNumber("Camera Rotate Speed", rotate);
    SmartDashboard.putNumber("Camera Drive Speed", speed);
    MathUtil.clamp(speed, -.3, .3);
    Robot.drive.drive(speed, rotate);



    // If camera can see the target
    /* if (Robot.vision.hasTarget()) {
      // Handle the angular error first
      double rotate = 0;
      double speed = 0;
      double rKP = .03;
      double sKP = .05;
      double error = Robot.vision.getAngle();

      if (Math.abs(error) > 1.5) {
        if (Math.abs(error) < 3) {
          rotate = error * rKP * 2;
        }
        else {
          rotate = error * rKP;
        }
        Robot.drive.drive(speed, rotate);
      } 
      else {
        // Handle the distance error
        double yError = Robot.vision.getDistance();
        speed = yError * sKP;
        rotate = 0;

        Robot.drive.drive(speed, rotate);
      }
    } */
  }

  public void end(boolean interupted) {
    System.out.println("Command end");
    Robot.drive.drive(0, 0);
  }

  public boolean isFinished() {
    return Robot.colorSensor0.hasBall();
  }
}