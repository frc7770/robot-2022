/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.drive;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class AutonomousLimeLightCommand extends CommandBase {
  double rKP = .075;
  double rKI = 0;
  double rKD = 0;
  PIDController rotateController = new PIDController(rKP, rKI, rKD);
  double sKP = .055;
  double sKI = 0;
  double sKD = 0;
  PIDController speedController = new PIDController(sKP, sKI, sKD);
  /**
   * Distance in feet
   * @param distance
   */
  public AutonomousLimeLightCommand() {
    System.out.println("Created AutonmousCamera");
    addRequirements(Robot.drive);
  }
  @Override
  public void execute() {
    double speed = 0;
    double rotate = 0; 
    if (Robot.limeLight.detectTarget()) {
      double rError = Robot.limeLight.getXAngle();
      double dError = Robot.limeLight.getYAngle();

      rotate = rotateController.calculate(-rError, 0);
      speed = speedController.calculate(-dError, 0);
    }
    SmartDashboard.putNumber("Limelight Drive Speed", speed);
    SmartDashboard.putNumber("LimeLight Rotate Speed", rotate);
    speed = MathUtil.clamp(speed, -.5, .5);
    Robot.drive.drive(speed, rotate);

    
    // If limelight can see the target
    /* if (Robot.limeLight.detectTarget()) {
      // Handle the angular error first
      double rotate = 0;
      double speed = 0;
      double rKP = .03;
      double sKP = .05;
      double error = Robot.limeLight.getXAngle();

      if (Math.abs(error) > 3) {
        if (Math.abs(error) < 4.5) {
          rotate = error * rKP * 2;
        }
        else {
          rotate = error * rKP;
        }
        Robot.drive.drive(speed, rotate);
      } 
      else {
        // Handle the distance error
        double yError = Robot.limeLight.getYAngle() - 20;
        speed = yError * sKP;
        rotate = 0;

        Robot.drive.drive(speed, rotate);
      }
    } */
  }

  public void end(boolean interrupted) {
    Robot.drive.drive(0, 0);
  }

  public boolean isFinished() {
    return Robot.limeLight.isSweetSpot() || !(Robot.limeLight.detectTarget());
  }
}