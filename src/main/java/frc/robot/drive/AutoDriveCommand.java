package frc.robot.drive;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class AutoDriveCommand extends CommandBase { 
    // Amount of angle we still need to turn
    private double error = 0;
    // Encoder reading from previous loop
    private double lastClick = 0;
    private double DISTANCE = 0;
    private static double KP = .75;

    public AutoDriveCommand(double distance) {
        addRequirements(Robot.drive);
        DISTANCE = distance;
        error = distance;
        System.out.println("AutoDriveCommand Constructor: " + distance);
    }

    @Override
    public void execute() {
        System.out.println("AutoDriveCommand Execute: " + error);
        if(lastClick==0) {
            lastClick = Robot.drive.getLeftEncoderPosition();
        }
        double speed = 0;
        double rotate = 0;
        // Get current encoder position
        double currentClick = Robot.drive.getLeftEncoderPosition();
        double clicks = currentClick - lastClick;
        double distanceMoved = clicks / Constants.DRIVE_ENCODER_RATIO;
        error = error - distanceMoved;

        if (Math.abs(error) > 0) {
            speed = error * KP;
        }
        speed = MathUtil.clamp(speed, -.4, .4);
        //else {
        System.out.println("AutoDriveCommand Execute Speed: " + speed);

        lastClick = currentClick;
        Robot.drive.drive(speed, rotate);
    }

    @Override
    public boolean isFinished() {
        return (Math.abs(this.error) < .2);
    }

    @Override
    public void end(boolean interrupted) {
        System.out.println("Command End");
        error = DISTANCE;
        lastClick = 0;
    }
}
