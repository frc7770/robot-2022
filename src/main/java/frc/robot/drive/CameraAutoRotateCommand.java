/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.drive;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class CameraAutoRotateCommand extends CommandBase {
  /**
   * Distance in feet
   * @param distance
   */
  public CameraAutoRotateCommand() {
    addRequirements(Robot.drive);
  }
  @Override
  public void execute() {
    double rotate = 0;
    double KP = .015;

    double error = Robot.vision.getAngle();
    rotate = error * KP;

    double speed = 0;

    Robot.drive.drive(speed, rotate);
    SmartDashboard.putNumber("Angle", error);
    SmartDashboard.putNumber("Rotate Speed", rotate);    
  }

  


  @Override
  public boolean isFinished() {
    return false;
  }
}