/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.drive;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

/**
 * Creating an arcade drive for a four motor base
 */
public class DriveSubsystem extends SubsystemBase 
{
  // Set the left drive motors
  private static final CANSparkMax leftMaster = new CANSparkMax(Constants.DRIVE_LEFT_FRONT, CANSparkMax.MotorType.kBrushless);
  private static final CANSparkMax leftSlave = new CANSparkMax(Constants.DRIVE_LEFT_BACK, CANSparkMax.MotorType.kBrushless);
  MotorControllerGroup m_left = new MotorControllerGroup(leftMaster, leftSlave);
  private static final RelativeEncoder leftMasterEncoder = leftMaster.getEncoder();
  private static final RelativeEncoder leftSlaveEncoder = leftSlave.getEncoder();
  // Set the right drive motors
  private static final CANSparkMax rightMaster = new CANSparkMax(Constants.DRIVE_RIGHT_FRONT, CANSparkMax.MotorType.kBrushless);
  private static final CANSparkMax rightSlave = new CANSparkMax(Constants.DRIVE_RIGHT_BACK, CANSparkMax.MotorType.kBrushless);
  MotorControllerGroup m_right = new MotorControllerGroup(rightMaster, rightSlave);
  private static final RelativeEncoder rightMasterEncoder = rightMaster.getEncoder();
  private static final RelativeEncoder rightSlaveEncoder = rightSlave.getEncoder();

  // Assign the drive motors to the DifferentialDrive
  private DifferentialDrive drive = new DifferentialDrive(m_left, m_right); 

  public DriveSubsystem()   {
    m_right.setInverted(true);
    rightMasterEncoder.setPosition(0);
    leftMasterEncoder.setPosition(0);
  }

  //public double Drive

  public void logToDashboard() {
    SmartDashboard.putNumber("LeftEncoder", leftMaster.getEncoder().getPosition()); 
    SmartDashboard.putNumber("RightEncoder", -rightMaster.getEncoder().getPosition());
  }

  /**
   * Set the speed and rotation for the drive system and account for a dead zone in the controller
   * @param speed The robots speed along the x-axis -1 to 1 forward is positive
   * @param rotate The robots rotation rate along the z-axis -1 to 1 clockwise is positive
   */
  public void drive(double speed, double rotate) {
    // Unsetting the speed and roation values
    double Zero = 0;
    // If both values are in the dead zone then set the speed to zero
    if (Math.abs(speed)<Constants.DRIVE_DEADBAND && Math.abs(rotate)<Constants.DRIVE_DEADBAND)
    {
      speed = Zero;
      drive.arcadeDrive(speed, rotate);
    }
    // If only one of the values is in the dead zone... 
    else if (Math.abs(speed)<Constants.DRIVE_DEADBAND || Math.abs(rotate)<Constants.DRIVE_DEADBAND)
    {  
      // ...If the speed is in the dead zone set the speed to zero  
      if (Math.abs(speed)<Constants.DRIVE_DEADBAND) {
       speed = Zero;
       drive.arcadeDrive(speed, rotate);
      }
      // ...If the rotation value is in the dead zone set the rotation to zero
      else {
        rotate = Zero; 
        drive.arcadeDrive(speed, rotate);
      } 
    } 
    // If both values are not in the dead zone use them
    else {
      drive.arcadeDrive(speed, rotate);
    }
  }

  public double getLeftEncoderPosition() {
    return leftMaster.getEncoder().getPosition();
  }
}