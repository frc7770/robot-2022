/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.drive;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class XboxDriveCommand extends CommandBase {
  public XboxDriveCommand() {
    addRequirements(Robot.drive);
  }
  @Override
  public void execute() {
    double rotate = .5 * Robot.xboxController1.getLeftX();
    double speed = .5 * -Robot.xboxController1.getLeftY();

    Robot.drive.drive(speed, rotate);
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
