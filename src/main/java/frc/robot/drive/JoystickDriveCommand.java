/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.drive;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class JoystickDriveCommand extends CommandBase {
  public JoystickDriveCommand() {
    addRequirements(Robot.drive);
  }
  @Override
  public void execute() {
    if (Robot.limeLight.getYAngle() < 20 && Robot.limeLight.getYAngle() > 10) {
      Robot.xboxController1.setRumble(RumbleType.kLeftRumble, (1.0 / (1 + Math.abs(15 - Robot.limeLight.getYAngle()))));
      Robot.xboxController1.setRumble(RumbleType.kRightRumble, (1.0 / (1 + Math.abs(15 - Robot.limeLight.getYAngle()))));
    }
    double rotate = .5 * Robot.joystick.getZ();
    double speed = Robot.joystick.getY();

    Robot.drive.drive(speed, rotate);
  }

  public void end(boolean interrupted) {
    Robot.xboxController1.setRumble(RumbleType.kLeftRumble, 0);
    Robot.xboxController1.setRumble(RumbleType.kRightRumble, 0);
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
