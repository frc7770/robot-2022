package frc.robot.drive;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class AutoRotateCommand extends CommandBase { 
    // Amount of angle we still need to turn
    private double error = 0;
    // Encoder reading from previous loop
    private double lastClick = 0;
    private double ANGLE = 0;
    private static double KP = .04;

    private boolean isFinished = false;
    public AutoRotateCommand(double angle) {
        addRequirements(Robot.drive);
        ANGLE = angle;
        error = angle;
        //System.out.println("AutoRotateCommand Constructor: " + angle);
    }

    @Override
    public void execute() {
        if(lastClick==0) {
            lastClick = Robot.drive.getLeftEncoderPosition();
        }
        //System.out.println("AutoRotateCommand Execute: " + error);
        double speed = 0;
        double rotate = 0;
        // Get current encoder position
        double currentClick = Robot.drive.getLeftEncoderPosition();
        double clicks = currentClick - lastClick;
        double angleMoved = clicks / Constants.DRIVE_ROTATION_ENCODER_RATIO;
        error = error - angleMoved;

        if (Math.abs(error) > 0) {
            rotate = error * KP;
        }

        // Slow the rotate to half speed
        rotate = MathUtil.clamp(rotate, -.5, .5);

        //else {
        System.out.println("AutoRotateCommand Execute Rotate: " + rotate);
        SmartDashboard.putNumber("Rotate Error", error);



        lastClick = currentClick;
        Robot.drive.drive(speed, rotate);
    }

    @Override
    public boolean isFinished() {
        return (Math.abs(this.error) < 1);
    }

    @Override
    public void end(boolean interrupted) {
        System.out.println("Command End");
        error = ANGLE;
        lastClick = 0;
    }
}
