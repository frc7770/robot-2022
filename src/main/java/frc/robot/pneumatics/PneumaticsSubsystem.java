package frc.robot.pneumatics;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class PneumaticsSubsystem extends SubsystemBase {
    private DoubleSolenoid intake = new DoubleSolenoid(Constants.PNEUMATIC_HUB_ID, PneumaticsModuleType.REVPH, 15, 0);
    private DoubleSolenoid climb = new DoubleSolenoid(Constants.PNEUMATIC_HUB_ID, PneumaticsModuleType.REVPH, 1, 14);
    private Compressor phCompressor = new Compressor(Constants.PNEUMATIC_HUB_ID, PneumaticsModuleType.REVPH);
    public PneumaticsSubsystem() {
        if (intake != null) {
            intake.set(DoubleSolenoid.Value.kForward);

        }
        if (climb != null) {
            climb.set(DoubleSolenoid.Value.kReverse); 
        } 
    }

    public void toggleIntake() {
        if (intake != null) {
            intake.toggle();
        }
        System.out.println("Toggle activate");
    }

    public void intakeDown() {
        if (intake != null) {
            intake.set(DoubleSolenoid.Value.kReverse);
        }
    }

    public void toggleClimber() {
        if (climb != null) {
            climb.toggle();
        }
        System.out.println("Toggle activate");
    }

    public void climberDown() {
        if (climb != null) {
            climb.set(DoubleSolenoid.Value.kForward);
        }
    }
    public void toggleCompressor() {
        if (phCompressor == null)
            return;
        if(phCompressor.enabled()) {
            phCompressor.disable();
        }
        else {
            phCompressor.enableDigital();
        }
    }

    public void logToDashboard() {
        SmartDashboard.putBoolean("Climber is Up", isClimberPneumaticsBackward());
        SmartDashboard.putBoolean("Intake is Up", isIntakeUp());
    }

    public boolean isClimberPneumaticsBackward() {
        return climb.get() == DoubleSolenoid.Value.kReverse;
    }

    public boolean isIntakeUp() {
        return intake.get() == DoubleSolenoid.Value.kForward;
    }
}