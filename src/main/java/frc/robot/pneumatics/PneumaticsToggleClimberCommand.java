package frc.robot.pneumatics;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class PneumaticsToggleClimberCommand extends CommandBase {
    public PneumaticsToggleClimberCommand() {
        addRequirements(Robot.pneumatics);

    }

    @Override
    public void execute() {
        Robot.pneumatics.toggleClimber();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
