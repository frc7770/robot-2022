package frc.robot.pneumatics;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class PneumaticsToggleCompressorCommand extends CommandBase {
    public PneumaticsToggleCompressorCommand() {
        addRequirements(Robot.pneumatics);
    }

    @Override
    public void execute() {
        Robot.pneumatics.toggleCompressor();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
