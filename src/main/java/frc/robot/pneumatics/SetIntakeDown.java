package frc.robot.pneumatics;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class SetIntakeDown extends CommandBase {
    public SetIntakeDown() {
        addRequirements(Robot.pneumatics);
    }

    @Override
    public void execute() {
        Robot.pneumatics.intakeDown();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}

