package frc.robot.pneumatics;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class PneumaticsToggleIntakeCommand extends CommandBase {
    public PneumaticsToggleIntakeCommand() {
        addRequirements(Robot.pneumatics);
    }

    @Override
    public void execute() {
        Robot.pneumatics.toggleIntake();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}

