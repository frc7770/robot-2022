package frc.robot.climber;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;
import frc.robot.Robot;
import edu.wpi.first.wpilibj2.command.SubsystemBase;


/**
 * Spins a pair of wheels that launches the ball at the target
 */
public class ClimberSubsystem extends SubsystemBase {
    public MotorControllerGroup m_climber;
    public CANSparkMax climberLeft;
    public CANSparkMax climberRight;
    public RelativeEncoder climberLeftEncoder;
    public RelativeEncoder climberRightEncoder;
    public double LEFT_PARTIAL_EXTENDED = -110;
    public double RIGHT_PARTIAL_EXTENDED = -100;
    public double LEFT_FULLY_EXTENDED = -135;
    public double RIGHT_FULLY_EXTENDED = -130;
    public double FULLY_RETRACTED = 20;

    public ClimberSubsystem() {
        climberLeft = new CANSparkMax(Constants.CLIMBER_LEFT, CANSparkMax.MotorType.kBrushless);
        climberLeft.setIdleMode(CANSparkMax.IdleMode.kBrake);
        climberRight = new CANSparkMax(Constants.CLIMBER_RIGHT, CANSparkMax.MotorType.kBrushless);
        climberRight.setIdleMode(CANSparkMax.IdleMode.kBrake);
        climberLeftEncoder = climberLeft.getEncoder();
        climberLeftEncoder.setPosition(0);
        climberRightEncoder = climberRight.getEncoder();
        climberRightEncoder.setPosition(0);
        climberRight.setInverted(true);
        //if ((climberLeft != null) && (climberRight != null)) {
          //  m_climber = new MotorControllerGroup(climberLeft, climberRight);
        //}
    }

    public void retract() {
        if (climberLeft != null && climberLeftEncoder.getPosition() < FULLY_RETRACTED) {
            climberLeft.set(.8 * Constants.CLIMBER_SPEED);
        }
        else {
            climberLeft.set(0);
        }
        if (climberRight != null && climberRightEncoder.getPosition() < FULLY_RETRACTED) {
            climberRight.set(.8 * Constants.CLIMBER_SPEED);
        }
        else {
            climberRight.set(0);
        }
    }

    public void slowExtend() {
        if(climberLeft != null && climberLeftEncoder.getPosition() < FULLY_RETRACTED && climberLeftEncoder.getPosition() > -20) {
            climberLeft.set(.25 * -Constants.CLIMBER_SPEED);
        }
        else if (climberLeft != null && climberLeftEncoder.getPosition() >= LEFT_PARTIAL_EXTENDED) {
            climberLeft.set(-Constants.CLIMBER_SPEED);
        }
        else if (climberLeft != null && climberLeftEncoder.getPosition() >= LEFT_FULLY_EXTENDED && !(Robot.pneumatics.isClimberPneumaticsBackward())) {
            climberLeft.set(-Constants.CLIMBER_SPEED);
        }
        else {
            climberLeft.set(0);
        }
        if(climberRight != null && climberRightEncoder.getPosition() < FULLY_RETRACTED && climberRightEncoder.getPosition() > -20) {
            climberRight.set(.25 * -Constants.CLIMBER_SPEED);
        }
        else if (climberRight != null && climberRightEncoder.getPosition() >= RIGHT_PARTIAL_EXTENDED) {
            climberRight.set(-Constants.CLIMBER_SPEED);
        }
        else if (climberRight != null && climberRightEncoder.getPosition() >= RIGHT_FULLY_EXTENDED && !(Robot.pneumatics.isClimberPneumaticsBackward())) {
            climberRight.set(-Constants.CLIMBER_SPEED);
        }
        else {
            climberRight.set(0);
        }
    }
    public void stop() {
        climberLeft.set(0);
        climberRight.set(0);
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Climber Left Encoder", climberLeftEncoder.getPosition());
        SmartDashboard.putNumber("Climber Right Encoder", climberRightEncoder.getPosition());
    }

    public void setRightSpeed(double speed) {
        climberRight.set(speed);
    }

    public void setLeftSpeed(double speed) {
        climberLeft.set(speed);
    }

    public double getLeftEncoderPosition() {
        return climberLeftEncoder.getPosition();
    }

    public double getRightEncoderPosition() {
        return climberRightEncoder.getPosition();
    }

    public void setEncoderPositionZero() {
        climberLeftEncoder.setPosition(0);
        climberRightEncoder.setPosition(0);
    }
}

