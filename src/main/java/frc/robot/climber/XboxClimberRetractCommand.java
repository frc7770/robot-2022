/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.climber;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class XboxClimberRetractCommand extends CommandBase {
  private boolean isRunning = false;
  public XboxClimberRetractCommand() {
    addRequirements(Robot.climber);
  }
  @Override
  public void execute() {
    if (!(isRunning)) {
      Robot.climber.retract();
      isRunning = true;
    }
    else {
      isRunning = false;
    }
  }

  //@Override
  //public boolean isFinished() {
    ///return !(Robot.climber.isRightInRange() && Robot.climber.isLeftInRange());
  //}

  public void end() {
    Robot.climber.stop();;
  }
}
