package frc.robot.climber;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class ZeroClimberPositionCommand extends CommandBase {
    public ZeroClimberPositionCommand() {
        addRequirements(Robot.climber);
    }

    public void execute() {
        if (Robot.climber.getLeftEncoderPosition() != 0) {
            Robot.climber.setEncoderPositionZero();
        }
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}