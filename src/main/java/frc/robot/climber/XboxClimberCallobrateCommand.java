package frc.robot.climber;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class XboxClimberCallobrateCommand extends CommandBase {
    public XboxClimberCallobrateCommand() {
        addRequirements(Robot.climber);
        System.out.println("Callobration Start");
    }
    @Override
    public void execute() {
        while (Robot.joystick.getPOV() == 0) {
            Robot.climber.slowExtend();
        }
        while (Robot.joystick.getPOV() == 90) {
            Robot.climber.setRightSpeed(Constants.CLIMBER_SPEED);
        }
        while (Robot.joystick.getPOV() == 180) {
            Robot.climber.retract();
        }
        while (Robot.joystick.getPOV() == 270) {
            Robot.climber.setLeftSpeed(Constants.CLIMBER_SPEED);
        }
        double speed = .2 * Robot.xboxController1.getLeftY();
        double speed2 = .2 * Robot.xboxController1.getRightY();
        Robot.climber.setLeftSpeed(speed);
        Robot.climber.setRightSpeed(speed2);
        //SmartDashboard.putNumber("Left Callobration Speed", speed);
        //SmartDashboard.putNumber("Right Callobration Speed", speed2);
        //SmartDashboard.putNumber("JoystickPOV ", Robot.joystick.getPOV());
    }
    
    @Override
    public boolean isFinished() {
       return false;
    }
}

