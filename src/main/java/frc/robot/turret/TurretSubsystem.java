package frc.robot.turret;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class TurretSubsystem extends SubsystemBase {
    public CANSparkMax turretMaster;
    public RelativeEncoder turretMasterEncoder;

    public TurretSubsystem() {
        turretMaster = new CANSparkMax(Constants.TURRET, CANSparkMax.MotorType.kBrushless);
        if (turretMaster != null) {
           turretMasterEncoder = turretMaster.getEncoder();
           turretMasterEncoder.setPosition(0);
        }
    }

    public void setSpeed(double speed) {
        if (turretMaster != null) {
            if (turretMasterEncoder.getPosition() < 1000 && turretMasterEncoder.getPosition() > -1000) {
                turretMaster.set(speed * .4);
            }
            else {
                turretMaster.set(0);
            }
            SmartDashboard.putNumber("Turret Speed", speed);
        }
    }

    public double getEncoderPosition() {
        if (turretMasterEncoder != null) {
            return turretMasterEncoder.getPosition();
        }
        return 0;
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Turret Encoder Position", getEncoderPosition());
    }
}
