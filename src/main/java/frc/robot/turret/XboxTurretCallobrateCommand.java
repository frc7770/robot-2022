package frc.robot.turret;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class XboxTurretCallobrateCommand extends CommandBase {
    public XboxTurretCallobrateCommand() {
        addRequirements(Robot.turret);
        System.out.println("Callobration Start");
    }
    @Override
    public void execute() {
        double speed = Robot.xboxController1.getLeftTriggerAxis();
        double speed2 = Robot.xboxController1.getRightTriggerAxis();
        if (speed2 < .1 && speed > .1) {
            Robot.turret.setSpeed(speed);
        }
        else if (speed2 > .1 && speed < .1) {
            Robot.turret.setSpeed(-speed2);
        }
        else {
            Robot.turret.setSpeed(0);
        }
        SmartDashboard.putNumber("Left Rotation", speed);
        SmartDashboard.putNumber("Right Rotation", speed2);
    }
    
    @Override
    public boolean isFinished() {
       return false;
    }
}


