/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.turret;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class XboxTurretCommand extends CommandBase {
  public XboxTurretCommand() {
    addRequirements(Robot.turret);
  }
  @Override
  public void execute() {
    double rightJoystickX = Robot.xboxController1.getRightX();
    
    Robot.turret.setSpeed(rightJoystickX); 
    SmartDashboard.putNumber("Right JoystickX", rightJoystickX);
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
