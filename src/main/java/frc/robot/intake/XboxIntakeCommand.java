/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.intake;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class XboxIntakeCommand extends CommandBase {
  private boolean isRunning = false;
  public XboxIntakeCommand() {
    addRequirements(Robot.intake);
  }
  @Override
  public void execute() {
    if (!(isRunning)) {
      Robot.intake.setSpeed();
      isRunning = true;
    }
    else {
      Robot.intake.stop();
      isRunning = false;
    }
    //boolean pressLB = Robot.xboxController1.getLeftBumper();
    //double leftTrigger = Robot.xboxController1.getLeftTriggerAxis();


  }
  @Override
  public boolean isFinished() {
    return true;
  }
}
