/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.intake;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.sensors.ColorSensor;

public class NewAutonomousIntakeCommand extends CommandBase {
  public NewAutonomousIntakeCommand() {
    addRequirements(Robot.intake);
  }
  @Override
  public void execute() {
    if (!(Robot.pneumatics.isIntakeUp())) {
      Robot.intake.setSpeed();
    }
  }

  @Override
  public void end(boolean interrupted) {
    Robot.intake.stop();
  }
}
