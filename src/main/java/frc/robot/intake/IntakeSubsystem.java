package frc.robot.intake;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class IntakeSubsystem extends SubsystemBase {
    public boolean isRunning = false;
    public VictorSPX intakeMotor;
    public CANSparkMax newIntakeMotor;
    public RelativeEncoder intakeEncoder;

    public IntakeSubsystem() {
        //intakeMotor = new VictorSPX(Constants.INTAKE_MOTOR);
        newIntakeMotor = new CANSparkMax(Constants.INTAKE_MOTOR, CANSparkMax.MotorType.kBrushless);
    }
    public void setSpeed() {
        if (newIntakeMotor != null) {
            //intakeMotor.set(ControlMode.PercentOutput, -Constants.INTAKE_SPEED);
            newIntakeMotor.set(Constants.INTAKE_SPEED);
            isRunning = true;
        }
    }

    public void setReverse() {
        if (newIntakeMotor != null) {
            //intakeMotor.set(ControlMode.PercentOutput, Constants.INTAKE_SPEED);
            newIntakeMotor.set(-Constants.INTAKE_SPEED);
            isRunning = true;
        }
    }

    public void stop() {
        if (newIntakeMotor != null) {
            //intakeMotor.set(ControlMode.PercentOutput, 0);
            newIntakeMotor.set(0);
            isRunning = false;
        }
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void logToDashboard() {
        if (newIntakeMotor != null) {
            //SmartDashboard.putNumber("Intake motor speed", intakeEncoder.getVelocity());
        }
    }
}

