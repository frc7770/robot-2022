/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.esophagus;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.sensors.ColorSensor;

public class NewAutonomousLoadCommand extends CommandBase {
  public NewAutonomousLoadCommand() {
    addRequirements(Robot.esophagus);
  }
  @Override
  public void execute() {
    if (Robot.intake.isRunning()) {
      Robot.esophagus.slowSetSpeed();
    }
    else {
      Robot.esophagus.stop();
    }
  }
}


