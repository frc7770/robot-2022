/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.esophagus;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class StopEsophagusCommand extends CommandBase {
  public StopEsophagusCommand() {
    addRequirements(Robot.esophagus);
  }
  @Override
  public void execute() {
    Robot.esophagus.stop();
  }

  @Override
  public void end(boolean interrupted) {
      Robot.esophagus.stop();
  }

  @Override
  public boolean isFinished() {
    return true;
  }
}


