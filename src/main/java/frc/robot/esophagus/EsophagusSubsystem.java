package frc.robot.esophagus;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class EsophagusSubsystem extends SubsystemBase {
    public CANSparkMax esophagusMotor;
    public RelativeEncoder esophagusEncoder;

    public EsophagusSubsystem() {
        esophagusMotor = new CANSparkMax(Constants.ESOPHAGUS_MOTOR, CANSparkMax.MotorType.kBrushless);
        if (esophagusMotor != null) {
            esophagusEncoder = esophagusMotor.getEncoder();
        }
    }

    
    public void setSpeed() {
        if (esophagusMotor != null) {
            esophagusMotor.set(Constants.ESOPHAGUS_SPEED); //Constants.ESOPHAGUS_SPEED
            SmartDashboard.putNumber("Encoder", esophagusMotor.getEncoder().getVelocity());
        }
    }

    public void stop() {
        esophagusMotor.set(0);
    }

    public void slowSetSpeed() {
        esophagusMotor.set(.25 * Constants.ESOPHAGUS_SPEED);
    }

    public void setReverse() {
        esophagusMotor.set(.25 * -Constants.ESOPHAGUS_SPEED);
    }

    public void setLoadSpeed() {
        esophagusMotor.set(.5 * Constants.ESOPHAGUS_SPEED);
    }

    public void setShotSpeed() {
        esophagusMotor.set(.6 * Constants.ESOPHAGUS_SPEED);
    }
}
