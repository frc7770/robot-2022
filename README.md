# Robot 2022

Rapid React


## Design and Challenge

Each alliance is assigned a cargo color (red or blue, based on alliance affiliation) to process by retrieving their assigned cargo and scoring it into the hub. Human players assist the cargo retrieval and scoring efforts from within their terminals. In the final moments of each match, alliance robots race to engage with their hangar to prepare for transport!

Each match begins with a 15-second autonomous period, during which time alliance robots operate only on pre-programmed instructions to score points by:
- taxiing from their tarmac and
- retrieving and scoring their assigned cargo into the hub.

In the final 2 minutes and 15 seconds of the match, drivers take control of the robots and score points by:
- continuing to retrieve and score their assigned cargo into the hub and
- engaging with their hangar.

The alliance with the highest score at the end of the match wins!

## Electric Subsystems

Subsystems are created for each dynamic system that uses electric motors or pneumatic rams.

### Drive

The drive system is a six wheel setup that uses a pair of SPARK MAX controllers with motors and encoders.  It uses the Arcade Drive model which gives the driver the ability to set values for forward and reverse motion and rotation.

### Intake

The intake is a bar on the front of the robot that draws in and centers the payload balls.  It uses a single VictorSPX, motor, and encoder setup.  It usually spins in the inward direction, but can be reversed.

### Esophagus

The esopagus is the pulley system that moves the payload from the intake to the shooter.  It uses a single SPARK MAX, motor, and encoder system.  It usually drives payload up but can be reversed.  The team uses a pair of color sensors to detect the payload and start and stop the pulleys.

### Turret

The turret is a pulley driven turn table that rotates the shooter outlet towards the target.  It uses a single SPARK MAX, motor, and encoder system.  It drives in both directions and needs to have stops to prevent over rotation.  The team uses a Limelight to detect the target and align the turret.

### Shooter

The shooter is the flywheel system used to propel the payload toward the target.  It uses a pair of SPARK MAX controllers with motors and encoders.  The wheels are mounted on opposite sides of the outlet and thus drive in opposite directions.  The system will always drive outwards.  The speed is modulated to control the trajectory of the shot.

### Climber

The climber arm extension is controlled by a SPARK MAX, motor, and encoder group on each arm.  The driver extends and retracts them as a group.

## Pneumatic Subsystems

### Intake Arm

The intake is a bar on the front of the robot that draws in and centers the payload balls.  A pair of pneumatic rams actuate the arm to extend and retract.

### Climber Arms

The climber arms have 30 degress of rotation capabilities driven by a pneumatic ram on each side.

## Sensors

### Limelight

Limelight is a plug-and-play smart camera purpose-built for the FIRST Robotics Competition.  Vision control pipelines are created using the included software to tune the camera to recognize a given target.  The system then retrns a tranjectory to that target.  7770 uses the limelight to automate the targetting of the turret and speed of the shooter.

https://limelightvision.io/

### Color Sensor

Describe what the sytem was supposed to do.

https://www.revrobotics.com/rev-31-1557/

This sensor can be used to read and compare colors, and also has a built-in IR (optical) Proximity Sensor and white LED for active target lighting. Supports High Speed I2C Communication (400kHz) as well as auto increment register read which allows the user to return all the color register and status register data in one read command instead of 4 separate read commands.

Version 3 of the REV Color Sensor introduces a new sensor chip by Broadcom due to the end of life of the V1/V2 sensor. Physical form factor is the same as V2, however there are some minor changes to the FTC SDK. Be sure to update to the latest SDK and configure your robot to use the "REV Color Sensor V3". Color values will not be consistent between V2 and V3 sensors.

## Driver Controls

Describe the controls and strategies for the drivers.

## Vendor Dependencies

### Vendor Library A

- [REVLib](https://docs.revrobotics.com/sparkmax/software-resources/spark-max-api-information#c++-and-java) - [https://software-metadata.revrobotics.com/REVLib.json](https://software-metadata.revrobotics.com/REVLib.json)


